// Задание:
// Написать реализацию кнопки "Показать пароль".
//
// Технические требования:
// В файле index.html лежит разметка для двух полей ввода пароля.
// По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь,
// иконка меняет свой внешний вид.
// Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
// Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
// По нажатию на кнопку "Подтвердить", нужно сравнить введенные значения в полях
// Если значения совпадают - вывести модальное окно (можно alert) с текстом - "You are welcome";
// Если значение не совпадают - вывести под вторым полем текст красного цвета  "Нужно ввести одинаковые значения"
// После нажатия на кнопку страница не должна перезагружаться
// Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.

console.log(' --------------- JS_Homework#10 --------------- ');

const inputs = document.getElementsByTagName('input'); //console.log(inputs);
const btnElement = document.querySelector('.btn');
const pElement = document.createElement('p');
pElement.innerHTML = `Нужно ввести одинаковые значения!<i class="far fa-frown icon-message"></i>`;
pElement.hidden = true;
pElement.classList.add('message');
document.querySelector(".password-form").insertBefore(pElement, btnElement);

// ------------- modal window -----------------
const modalWindow = document.querySelector("#modalWindow");
const modalOverlay = document.querySelector("#overlayAroundModalWindow");
const closeButton = document.querySelector("#closeBtn");

const is = document.getElementsByTagName('i'); //console.log(is);
for (let i = 0; i < is.length; i++) {
    is[i].addEventListener('click', function () {
        this.classList.toggle('fa-eye-slash');
        if (this.classList.contains('fa-eye-slash')) { // 'fa-eye-slash' - password is shown
            this.previousElementSibling.setAttribute('type', 'text');
        } else if (this.classList.contains('fa-eye')) { // 'fa-eye' - password is hidden
            this.previousElementSibling.setAttribute('type', 'password');
        }
    });
}
const modalContentFace = document.querySelector(`.modalWindowContent .far`);
btnElement.addEventListener('click', function () {
    pElement.hidden = true;
    if (inputs[0].value !== '' && inputs[0].value !== null) {
        if (inputs[0].value === inputs[1].value) {
            modalContentFace.classList.remove(`fa-meh`);
            modalContentFace.classList.add(`fa-smile`);
            document.querySelector(`.modalWindowText`).innerHTML = `You are welcome!`;
            modalWindow.classList.toggle("comeOnPopUp");
            modalOverlay.classList.toggle("comeOnPopUp");
        } else {
            pElement.hidden = false;
        }
    } else {
        modalContentFace.classList.remove(`fa-smile`);
        modalContentFace.classList.add(`fa-meh`);
        document.querySelector(`.modalWindowText`).innerHTML = `Please, enter something!`;
        modalWindow.classList.toggle("comeOnPopUp");
        modalOverlay.classList.toggle("comeOnPopUp");
    }
});
closeButton.addEventListener("click", function () {
    modalWindow.classList.toggle("comeOnPopUp");
    modalOverlay.classList.toggle("comeOnPopUp");
});